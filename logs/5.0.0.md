## 5.0.0
* Updated Drupal core to 8.8.0.
* Fixed all known deprecations. (Issue #3096212)
* Updated Metatag to 1.10.
* Updated Pathauto to 1.6.
