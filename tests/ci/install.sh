#!/usr/bin/env bash

# NAME
#     install.sh - Install CI dependencies.
#
# SYNOPSIS
#     install.sh
#
# DESCRIPTION
#     Install Missing Dependencies


# Make bash resolve paths from the location of this script, not the CWD (current
# working directory) of the user or process that called it.
cd "$(dirname "$0")" || exit 1

# Reuse ORCA's own includes for its $PATH additions and environment variables.
source ../../../orca/bin/ci/_includes.sh || exit 1

[[ -d "$ORCA_FIXTURE_DIR" ]] && composer require drupal/console drupal/drupal-extension --working-dir "$ORCA_FIXTURE_DIR"
exit 0